function getRulesOf(content) {
  let rules = {
    retweet: false,
    tagAFriend: false,
    follow: false
  };

  content = content.toLowerCase();
  if (content.indexOf("rt") != -1 || content.indexOf("retweet") != -1) {
    rules.retweet = true;
  }

  if (content.indexOf("tag") != -1) {
    rules.tagAFriend = true;
  }

  if (content.indexOf("follow") != -1) {
    rules.follow = true;
  }

  return true;
}

let tweet =
  "#spiderman #giveaway RT, Tag a friend, and following me and tweet me your favorite spiderman villian!";

getRulesOf(tweet);
