var fs = require("fs");
var path = require("path");
const logger = require("./util/logger");
var Twit = require("twit");
var config = require("./config");

var T = new Twit(config);

function directMessage(id, tweet) {
  let package = {
    event: {
      type: "message_create",
      message_create: {
        target: {
          recipient_id: id
        },
        message_data: {
          text: tweet
        }
      }
    }
  };

  T.post("direct_messages/events/new", package);
}

function followRules(rules, tweet) {
  let thisID = tweet.id_str;
  if (rules.favorite) {
    T.post("favorites/create", { id: thisID }, (err, res) => {
      if (err) console.log(err);
      console.log("followed");
    });
  }

  if (rules.retweet) {
    T.post(
      "statuses/retweet/:id",
      {
        id: thisID
      },
      (err, res) => {
        if (err) console.log(err);
        console.log("retweeted");
      }
    );
  }

  if (rules.follow) {
    T.post("friendships/create", { id: tweet.user.id }, (err, data) => {
      if (err) console.log(err);
      console.log("followed");
    });
  }

  if (rules.tagAFriend) {
    let tweetContent = `I wanna win! @al`;
    T.post("statuses/update", tweetContent, (err, data, res) => {
      if (err) console.log(err);
      console.log("tagged");
    });
  }

  let sendToAlexa =
    tweet.user.screen_name +
    " tweeted, and your JerseyGlamor entered the contest" +
    JSON.stringify(rules);

  directMessage("200950624", sendToAlexa);
}

function getRulesOf(content) {
  let rules = {
    retweet: false,
    tagAFriend: false,
    follow: false,
    favorite: false
  };

  content = content.toLowerCase();
  if (content.indexOf("rt") != -1 || content.indexOf("retweet") != -1) {
    rules.retweet = true;
  } else {
    console.log(content);
  }

  if (content.indexOf("tag") != -1) {
    rules.tagAFriend = true;
  }

  if (content.indexOf("follow") != -1) {
    rules.follow = true;
  }

  if (content.indexOf("favorite") != -1) {
    rules.favorite = true;
  }

  return true;
}

let userarray = [
  "29358147",
  "3889467165",
  "152432990",
  "869373187204669441",
  "2288148272",
  "129307263",
  "4141942394",
  "809926392418889729",
  "304478707",
  "369238299",
  "740664096400527361",
  "18236230",
  "25116299",
  "1717161709",
  "3041173947",
  "23725989",
  "3257160283",
  "2900995954",
  "2748585072",
  "327235149",
  "4038362244",
  "41707730",
  "19294897"
];

let gid = T.stream("statuses/filter", {
  follow: userarray
});

gid.on("tweet", tweetEvent);

function tweetEvent(eventMsg) {
  var replyto = eventMsg.in_reply_to_screen_name;
  var text = eventMsg.text;
  var from = eventMsg.user.screen_name;

  console.log(text, "recieved", from);
  console.log(eventMsg);
}

gid.on("tweet", tweet => {
  // console.log(tweet);
  let content = tweet.text;
  let hashtags = tweet.entities.hashtags;

  if (content.includes("@")) {
    console.log(content, "recieved and added to the stream")
    let userArr = content.split(/@/g)
    console.log(userArr)
  }

  if (!userarray.includes(tweet.user.id)) {
    console.log("diverted, invalid user... probably a retweet");
    return;
  }

  let giveaway = hashtags.filter(entity => {
    if (entity.text.includes("giveaway")) {
      return true;
    }
  });

  if (!giveaway.length > 0) {
    console.log("diverted");
    return;
  }

  let rules = getRulesOf(content);
  followRules(rules, tweet);
});

gid.on("error", error => {
  console.log(error);
});
